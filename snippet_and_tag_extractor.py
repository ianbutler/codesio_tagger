from lxml import etree as et
from lxml import html
from os import environ as env
from pathlib import Path
import dataset

def format_tags(tags_string):
    return list(map(lambda tag: tag.strip("<"), tags_string.strip().split(">")))[:-1]

db = dataset.connect('postgresql://localhost:5433/codesio_autotagger_dataset')
posts_table = db["posts"]

infile_path = Path(env["BASE_DIR"], "Posts.xml")

context = et.iterparse(str(infile_path), events=('end',), tag="row")
for event, row in context:
    try:
        body               = row.get("Body") or ""
        body               = list(map(str.strip, html.fromstring(body).xpath("//code/text()")))
        votes              = row.get("Score") or 0
        tags               = format_tags(row.get("Tags") or "")
        snippet            = "\n".join(body)
        accepted_answer_id = row.get("AcceptedAnswerId")
        original_id        = row.get("Id")
        posts_table.insert(dict(snippet=snippet, tags=tags, accepted_answer_id=accepted_answer_id, votes=votes, original_id=original_id))
    except:
        pass
    finally:
        row.clear()
