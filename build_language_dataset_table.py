import dataset
from collections import Counter

db = dataset.connect("postgresql://localhost:5432/language_tagging_db", reflect_views=True)
db["language_tagging_dataset_table"].drop()

tagged_posts = db["tagged_post_set"]
language_ds  = db["language_tagging_dataset_table_large_sample"]

languages_check = ['java',
        'javascript',
        'php',
        'c#',
        'python',
        'c++',
        'c',
        'r',
        'objective-c',
        'ruby',
        'matlab',
        'scala',
        'bash',
        'perl'
        # 'vba',
        # 'powershell',
        # 'delphi',
        # 'haskell',
        # 'flex',
        # 'go',
        # 'typescript',
        # 'vbscript',
        # 'xslt',
        # 'lua',
        # 'prolog',
        # 'sas',
        # 'groovy',
        # 'clojure',
        # 'coldfusion',
        # 'cuda',
        # 'dart',
        # 'erlang',
        # 'xpath',
        # 'fortran',
        # 'f#',
        # 'curl',
        # 'vhdl',
        # 'verilog',
        # 'awk',
        # 'chef',
        # 'tcl',
        # 'scheme',
        # 'latex',
        # 'applescript',
        # 'rust',
        # 'elixir',
        # 'sed',
        # 'opencl',
        # 'netlogo',
        # 'ocaml',
        # 'lisp'
]


count = 0

language_count = Counter()

def resultIter(table, arraysize=1000):
    def returnNextBatch(table, offset, limit=arraysize):
        while True:
            offset += limit
            results = table.all(_offset=offset, _limit=limit)

            if not results:
                break
            yield results
    offset = 0
    return returnNextBatch(table, offset)

def format_tag(tag):
    return tag.lstrip('{').rstrip('}').lower().strip()

result_iterator = resultIter(tagged_posts)

while True:
    posts = next(result_iterator)

    if not posts:
        break

    for post in posts:
        tags = list(map(format_tag, post["tags"].split(",")))
        languages = list(filter(lambda x: x.lstrip().rstrip().lower() in languages_check, tags))
        print(tags)
        print(languages)
        language = None

        if len(languages) == 0:
            continue
        elif len(languages) > 1:
            language = ",".join(languages)
        else:
            language = languages[0]

        if not language:
            continue

        if language_count[languages[0]] >= 50000:
            continue
        else:
            language_count[languages[0]] += 1


        count += 1

        language_ds.insert(dict(post_id=post['id'], language=language))

print(f"Inserted: {count}")



