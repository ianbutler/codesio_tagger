import numpy as np
import pandas as pd
from IPython import embed
import nltk
from matplotlib import pyplot as plt

df = pd.read_sql(
        "SELECT snippet, language FROM language_tagging_dataset_table lds JOIN tagged_post_set t ON t.id=lds.post_id WHERE snippet IS NOT NULL;",
        "postgresql://localhost:5432/language_tagging_db"
        )

snippets = df["snippet"]

full_text = "\n".join(list(snippets))

freq_dist = nltk.FreqDist(full_text)

labels, values = zip(*freq_dist.most_common(100))

plt.plot(labels, np.asarray(values) / 100)
plt.show()

print(labels)
