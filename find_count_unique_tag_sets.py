import csv
import hashlib

tag_sets = {}
def format_tag(tag):
    return tag.lstrip('{').rstrip('}').lower().strip()

with open("./tag_sets.csv", "r") as f:
    reader = csv.reader(f)
    for line in reader:
        elements = map(format_tag, line)
        values = sorted(elements)
        key = hashlib.sha1(','.join(values).encode("utf-8")).hexdigest()
        tag_sets[key] = values

print(len(tag_sets.keys()))
