import tensorflow as tf
from tensorflow import keras as k
from tensorflow.keras import backend as K
import numpy as np
import pandas as pd
import re
import faker
import datetime
import os.path
from IPython import embed
from gensim.models import Word2Vec

fake = faker.Faker()

fake.add_provider(faker.providers.color)

BATCH_SIZE = 256
VOCAB = [' ', 'e', 't', 'a', 'r', 'o', 'i', 'n', 's', '\n', 'l', 'c', 'd', 'p', 'u', 'm', '.', '"', '(', ')', '-', 'f', '0', 'h', 'g', '=', '/', '1', ':', ',', 'b', '_', 'y', ';', 'v', '2', 'x', '>', 'S', '<', 'w', 'C', 'T', 'E', "'", 'A', 'I', 'k', '3', 'D', 'R', 'L', 'N', '4', '[', ']', 'P', 'O', '5', 'M', '{', '*', '}', 'F', '6', '8', '$', 'B', '7', '9', 'j', '\\', 'U', '#', 'G', '+', 'q', 'H', 'V', 'W', 'z', '&', '|', '%', 'X', '!', '@', 'K', 'Y', '?', '`', 'Q', 'J', '~', 'Z', '^', '’', '─', '‘', '”']
SUBSTITUTION_STRING = "".join(VOCAB)

REVERSE_VOCAB = {v: i for i,v in enumerate(VOCAB)}

LANGUAGES = ["bash", "c", "c#", "c++", "java", "javascript", "matlab", "objective-c", "perl", "php", "python", "r", "ruby", "scala"]

LABEL_LOOKUP_TABLE = { v: i for i,v in enumerate(LANGUAGES) }

def remove_unwanted_characters(code_sample):
    pattern = "[^%s]" % SUBSTITUTION_STRING

    pattern = re.escape(pattern)

    return re.sub(pattern, "", code_sample)

def encode_labels_for_classification(label_df):
    return label_df.apply(lambda x: LABEL_LOOKUP_TABLE[x])

def translate_text_to_indices(sample_df):
    def map_to_index(value):
        return REVERSE_VOCAB.get(value, len(VOCAB) + 1)

    return sample_df.apply(lambda x: list(map(map_to_index, list(x)))[:250])

def prepare_for_embedding(df):
    return (df.apply(lambda x: list(remove_unwanted_characters(x))[:250])).tolist()

def prepare_samples_for_use(df, name):
    if os.path.isfile("./datasets/prepared_%s_x_set.df" % name):
        df = pd.read_pickle("./datasets/prepared_%s_x_set.df" % name)
    else:
        df = df.apply(lambda x: remove_unwanted_characters(x))
        df = translate_text_to_indices(df)
        df.to_pickle("./datasets/prepared_%s_x_set.df" % name)

    return np.array(df.tolist())

def prepare_labels_for_use(df, name):
    if os.path.isfile("./datasets/prepared_%s_y_set.df" % name):
        df = pd.read_pickle("./datasets/prepared_%s_y_set.df" % name)
    else:
        df = encode_labels_for_classification(df)
        df.to_pickle("./datasets/prepared_%s_y_set.df" % name)

    df_values = df.values

    return k.utils.to_categorical(df_values, len(LANGUAGES))

def extract_embedding_weights(wv):
    matrix = np.zeros((len(VOCAB) + 2, 100))

    for i in range(len(VOCAB)):
        embedding_vector = wv[VOCAB[i]]
        if embedding_vector is not None:
            matrix[i] = embedding_vector

    return matrix


def build_model(embedding_weights):
    model = k.models.Sequential()
    model.add(k.layers.Embedding(len(VOCAB) + 2, 100, input_length=250, weights=[embedding_weights]))
    model.add(k.layers.LSTM(25000))
    model.add(k.layers.Reshape([250,100]))
    model.add(k.layers.Conv1D(256, 8, activation='relu'))
    model.add(k.layers.MaxPooling1D(5))
    model.add(k.layers.Conv1D(256, 2, activation='relu'))
    model.add(k.layers.MaxPooling1D(5))
    model.add(k.layers.Conv1D(256, 2, activation='relu'))
    model.add(k.layers.MaxPooling1D(2))
    model.add(k.layers.Flatten())
    model.add(k.layers.Dense(256, activation='relu'))
    model.add(k.layers.Dense(len(LANGUAGES), activation='softmax'))
    return model

seed = 8679607

np.random.seed(seed)

if os.path.isfile("./datasets/dataset.df"):
    df = pd.read_pickle("./datasets/dataset.df")
else:
    df = pd.read_sql(
            "SELECT * FROM valid_sample_set_large WHERE char_length(snippet) >= 300",
            "postgresql://localhost:5432/language_tagging_db"
            )
    df.to_pickle("./datasets/dataset.df")

print(df['snippet'].describe())

if os.path.isfile("./datasets/w2vec.model"):
    word2vec_embeddings_model = Word2Vec.load("./datasets/w2vec.model")
else:
    embedding_sentences = prepare_for_embedding(df['snippet'])
    word2vec_embeddings_model = Word2Vec(embedding_sentences, size=100, window=10, workers=16, sg=0, negative=0)
    word2vec_embeddings_model.save("./datasets/w2vec.model")

if os.path.isfile("./datasets/embedding_weights.npy"):
    embedding_weights = np.load("./datasets/embedding_weights.npy")
else:
    word_vectors = word2vec_embeddings_model.wv
    embedding_weights = extract_embedding_weights(word_vectors)
    np.save("./datasets/embedding_weights.npy", embedding_weights)

mask = np.random.randn(len(df)) < 0.75

train_df = df[mask]
x_train = prepare_samples_for_use(train_df["snippet"], "train")
y_train = prepare_labels_for_use(train_df["language"], "train")

test_df = df[~mask]
x_test  = prepare_samples_for_use(test_df["snippet"], "test")
y_test  = prepare_labels_for_use(test_df["language"], "test")

model = build_model(embedding_weights)
model.summary()
model.compile(optimizer="sgd", loss="categorical_crossentropy", metrics=[k.metrics.categorical_accuracy])

tensorboard = k.callbacks.TensorBoard(log_dir="./tensorboard", batch_size=BATCH_SIZE, write_grads=True)

model.fit(x_train, y_train, epochs=100, batch_size=BATCH_SIZE, callbacks=[tensorboard])

timestamp = re.sub("\s", "_", str(datetime.datetime.today()))

k.models.save_model(model, "./models/{0}_{1}".format(fake.color_name(), timestamp))
